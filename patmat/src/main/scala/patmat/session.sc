package patmat
import Huffman._

object session {

  val sampleTree = makeCodeTree(
    makeCodeTree(Leaf('x', 1), Leaf('e', 1)),
    Leaf('t', 2))                                 //> sampleTree  : patmat.Huffman.Fork = Fork(Fork(Leaf(x,1),Leaf(e,1),List(x, e)
                                                  //| ,2),Leaf(t,2),List(x, e, t),4)
  val str = "Given a text, it’s possible to calculate and build an optimal Huffman tree in the sense that the encoding of that text will be of the minimum possible length, meanwhile keeping all information (i.e., it is lossless)."
                                                  //> str  : String = Given a text, it’s possible to calculate and build an opti
                                                  //| mal Huffman tree in the sense that the encoding of that text will be of the 
                                                  //| minimum possible length, meanwhile keeping all information (i.e., it is loss
                                                  //| less).
//  val tree = createCodeTree(string2Chars(str))
  val tree = createCodeTree(string2Chars("calculator"))
                                                  //> tree  : patmat.Huffman.CodeTree = Fork(Fork(Fork(Leaf(r,1),Leaf(o,1),List(r,
                                                  //|  o),2),Fork(Leaf(t,1),Leaf(u,1),List(t, u),2),List(r, o, t, u),4),Fork(Leaf(
                                                  //| c,2),Fork(Leaf(a,2),Leaf(l,2),List(a, l),4),List(c, a, l),6),List(r, o, t, u
                                                  //| , c, a, l),10)

  val encoded = encode(tree)(string2Chars("calculator"))
                                                  //> encoded  : List[patmat.Huffman.Bit] = List(1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 
                                                  //| 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0)
  decode(tree, encoded) mkString                  //> res0: String = calculator

  convert(tree)                                   //> res1: patmat.Huffman.CodeTable = List((t,List(0, 1, 0)), (u,List(0, 1, 1)), 
                                                  //| (r,List(0, 0, 0)), (o,List(0, 0, 1)), (a,List(1, 1, 0)), (l,List(1, 1, 1)), 
                                                  //| (c,List(1, 0)))
  val encoded2 = quickEncode(tree)(string2Chars("calculator"))
                                                  //> encoded2  : List[patmat.Huffman.Bit] = List(1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0,
                                                  //|  1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0)

}