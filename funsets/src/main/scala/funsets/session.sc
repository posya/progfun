package funsets

import FunSets._

object session {
  val s1 = singletonSet(1)                        //> s1  : Int => Boolean = <function1>
  val s2 = singletonSet(2)                        //> s2  : Int => Boolean = <function1>
  val s3 = singletonSet(3)                        //> s3  : Int => Boolean = <function1>
  val s4 = singletonSet(4)                        //> s4  : Int => Boolean = <function1>
  val s5 = singletonSet(5)                        //> s5  : Int => Boolean = <function1>
  val s6 = singletonSet(6)                        //> s6  : Int => Boolean = <function1>
  val s7 = singletonSet(7)                        //> s7  : Int => Boolean = <function1>
  val s8 = singletonSet(8)                        //> s8  : Int => Boolean = <function1>
  val odd = (x: Int) => x % 2 == 1                //> odd  : Int => Boolean = <function1>
  val lessThenFive = (x: Int) => x < 5            //> lessThenFive  : Int => Boolean = <function1>

  
  val s123 = union(s1, union(s2, s3))             //> s123  : Int => Boolean = <function1>
  
  val result = map(s123, x => x * 3)              //> result  : Int => Boolean = <function1>
  
  FunSets.toString(result)                        //> res0: String = {3,6,9}

  

}