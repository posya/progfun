package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def rec(c: Int, r: Int): Int = (c, r) match {
    case (c, r) if r < 0 || c < 0 || c > r => 0
    case (c, r) if c == 0 || c == r => 1
    case (c, r) => rec(c - 1, r - 1) + rec(c, r - 1)
  }

  def pascal(c: Int, r: Int): Int = {
    if (r < 0 || c < 0 || c > r) throw new Exception
    else rec(c, r)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {

    def loop(cs: List[Char], open: Int, closed: Int): Boolean = {
      if (closed > open) false
      else if (cs.isEmpty) (open == closed)

      else if (cs.head == '(') loop(cs.tail, open + 1, closed)
      else if (cs.head == ')') loop(cs.tail, open, closed + 1)
      else loop(cs.tail, open, closed)
    }

    loop(chars, 0, 0)
  }

  /**
   * Exercise 3
   */

  def countChange(money: Int, coins: List[Int]): Int = {
    if (money == 0) 1
    else if ((money < 0) || coins.isEmpty) 0
    else countChange(money, coins.tail) + countChange(money - coins.head, coins)
  }
}
